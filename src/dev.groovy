import jenkins.model.*
jenkins = Jenkins.instance

def profileVersion = new Date().format( 'dd.MM.yyyy.HH.mm' )

def call() {
	node {	
	  stage("Clone") {                         
		git credentialsId: 'git-credentials', url: 'https://gitlab.com/damarinm26/hcm-proxy'        
      }
	  stage("Build") {
		withMaven(
			maven: 'M3'){
				sh "mvn clean install -DskipTests"            
			}		        
      }
	  stage('Package') {
        sh 'mvn package -Pdev -DskipTests'                        
      }
	  stage('Deploy') {
        sh 'mvn deploy -Pdev -DskipTests'                        
      }
	  stage('Fabric8') {
		sh 'mvn clean -DskipTests=true fabric8:deploy -Dfabric8.jolokiaUrl=http://localhost:8181/jolokia -Dfabric8.profileVersion=${profileVersion}'  
	  }  
    }	
}

