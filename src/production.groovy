import jenkins.model.*
jenkins = Jenkins.instance

def call() {
	node {	
	  stage("Clone Repository") {                         
		git credentialsId: 'git_assert_credentials', url: 'https://assertsolutions.xp-dev.com/git/XM_Devops'        
      }
	  stage("Maven Build") {
        bat 'mvn -Dmaven.test.failure.ignore=true install'            
      }
	  stage('Deploy') {
        rtPublishBuildInfo (
                    serverId: "artifactory_local"
        )            
      }
    }
}

