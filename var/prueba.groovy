package var

def saludo() {
	echo "Hola estoy saludando desde prueba"
	pipeline {
    parameters {
        gitParameter branchFilter: 'origin/(.*)', defaultValue: 'master', name: 'BRANCH', type: 'PT_BRANCH'
    }
    agent any
    stages {
		stage("Build"){
			input{
					message "Press Ok to continue"
					submitter "user1,user2"
				parameters {
						string(name:'username', defaultValue: 'user', description: 'Username of the user pressing Ok')
				}
			}
			steps {
				script{
					new prueba().saludo()
				}
				echo "User: ${username} said Ok."
			}
		}
        stage("Clone Repository") {
            steps {               
				git branch: "${params.BRANCH}", credentialsId: 'git_assert_credentials', url: 'https://assertsolutions.xp-dev.com/git/XM_Devops'
            }
        }
        stage("Maven Build") {
            steps {
                bat 'mvn -Dmaven.test.failure.ignore=true install'
            }
        }
		stage('Deploy') {
            when {
              expression {
                currentBuild.result == null || currentBuild.result == 'SUCCESS' 
              }
            }
            steps {
                rtPublishBuildInfo (
                    serverId: "artifactory_local"
                )
            }
        }
    }
}
}

